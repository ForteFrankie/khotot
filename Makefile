CC = ./tools/generate.pe
RM = rm -f


#this stuff has to be included
ESSENTIALS=ae_fonts/COPYING ae_fonts/README

#font files
FONTS = ae_fonts/Kasr/ae_AlArabiya.ttf ae_fonts/Kasr/ae_AlBattar.ttf ae_fonts/Kasr/ae_AlHor.ttf ae_fonts/Kasr/ae_AlManzomah.ttf ae_fonts/Kasr/ae_AlMateen-Bold.ttf ae_fonts/Kasr/ae_AlMohanad.ttf ae_fonts/Kasr/ae_AlMothnna-Bold.ttf ae_fonts/Shmookh/ae_AlYarmook.ttf ae_fonts/FS/ae_Arab.ttf ae_fonts/AAHS/ae_Cortoba.ttf ae_fonts/AGA/ae_Dimnah.ttf ae_fonts/MCS/ae_Electron.ttf ae_fonts/AGA/ae_Furat.ttf ae_fonts/AGA/ae_Granada.ttf ae_fonts/FS/ae_Graph.ttf ae_fonts/Shmookh/ae_Hani.ttf ae_fonts/MCS/ae_Haramain.ttf ae_fonts/MCS/ae_Hor.ttf ae_fonts/FS/ae_Japan.ttf ae_fonts/FS/ae_Jet.ttf ae_fonts/AGA/ae_Kayrawan.ttf ae_fonts/Shmookh/ae_Khalid.ttf ae_fonts/AGA/ae_Mashq-Bold.ttf ae_fonts/AGA/ae_Mashq.ttf ae_fonts/FS/ae_Metal.ttf ae_fonts/AGA/ae_Nada.ttf ae_fonts/AAHS/ae_Nagham.ttf ae_fonts/FS/ae_Nice.ttf ae_fonts/AAHS/ae_Ostorah.ttf ae_fonts/Shmookh/ae_Ouhod-Bold.ttf ae_fonts/AGA/ae_Petra.ttf ae_fonts/AGA/ae_Rasheeq-Bold.ttf ae_fonts/Shmookh/ae_Rehan.ttf ae_fonts/FS/ae_Salem.ttf ae_fonts/FS/ae_Shado.ttf ae_fonts/Shmookh/ae_Sharjah.ttf ae_fonts/AGA/ae_Sindbad.ttf ae_fonts/Shmookh/ae_Tarablus.ttf ae_fonts/MCS/ae_Tholoth.ttf
#for creating the ttf dir
ttf_files=./ttf/ae_AlArabiya.ttf ./ttf/ae_AlBattar.ttf ./ttf/ae_AlHor.ttf ./ttf/ae_AlManzomah.ttf ./ttf/ae_AlMateen-Bold.ttf ./ttf/ae_AlMohanad.ttf ./ttf/ae_AlMothnna-Bold.ttf ./ttf/ae_AlYarmook.ttf ./ttf/ae_Arab.ttf ./ttf/ae_Cortoba.ttf ./ttf/ae_Dimnah.ttf ./ttf/ae_Electron.ttf ./ttf/ae_Furat.ttf ./ttf/ae_Granada.ttf ./ttf/ae_Graph.ttf ./ttf/ae_Hani.ttf ./ttf/ae_Haramain.ttf ./ttf/ae_Hor.ttf ./ttf/ae_Japan.ttf ./ttf/ae_Jet.ttf ./ttf/ae_Kayrawan.ttf ./ttf/ae_Khalid.ttf ./ttf/ae_Mashq-Bold.ttf ./ttf/ae_Mashq.ttf ./ttf/ae_Metal.ttf ./ttf/ae_Nada.ttf ./ttf/ae_Nagham.ttf ./ttf/ae_Nice.ttf ./ttf/ae_Ostorah.ttf ./ttf/ae_Ouhod-Bold.ttf ./ttf/ae_Petra.ttf ./ttf/ae_Rasheeq-Bold.ttf ./ttf/ae_Rehan.ttf ./ttf/ae_Salem.ttf ./ttf/ae_Shado.ttf ./ttf/ae_Sharjah.ttf ./ttf/ae_Sindbad.ttf ./ttf/ae_Tarablus.ttf ./ttf/ae_Tholoth.ttf 

#create the ttf files in ./ttf
fonts: $(ttf_files)
	
./ttf:
	mkdir -p ./ttf

./ttf/ae_AlArabiya.ttf : ./sfd/ae_AlArabiya.sfd ./ttf
	$(CC) ./sfd/ae_AlArabiya.sfd  ./ttf/ae_AlArabiya.ttf
	
./ttf/ae_AlBattar.ttf : ./sfd/ae_AlBattar.sfd ./ttf
	$(CC) ./sfd/ae_AlBattar.sfd  ./ttf/ae_AlBattar.ttf
	
./ttf/ae_AlHor.ttf : ./sfd/ae_AlHor.sfd ./ttf
	$(CC) ./sfd/ae_AlHor.sfd  ./ttf/ae_AlHor.ttf
	
./ttf/ae_AlManzomah.ttf : ./sfd/ae_AlManzomah.sfd ./ttf
	$(CC) ./sfd/ae_AlManzomah.sfd  ./ttf/ae_AlManzomah.ttf
	
./ttf/ae_AlMateen-Bold.ttf : ./sfd/ae_AlMateen-Bold.sfd ./ttf
	$(CC) ./sfd/ae_AlMateen-Bold.sfd  ./ttf/ae_AlMateen-Bold.ttf
	
./ttf/ae_AlMohanad.ttf : ./sfd/ae_AlMohanad.sfd ./ttf
	$(CC) ./sfd/ae_AlMohanad.sfd  ./ttf/ae_AlMohanad.ttf
	
./ttf/ae_AlMothnna-Bold.ttf : ./sfd/ae_AlMothnna-Bold.sfd ./ttf
	$(CC) ./sfd/ae_AlMothnna-Bold.sfd  ./ttf/ae_AlMothnna-Bold.ttf
	
./ttf/ae_AlYarmook.ttf : ./sfd/ae_AlYarmook.sfd ./ttf
	$(CC) ./sfd/ae_AlYarmook.sfd  ./ttf/ae_AlYarmook.ttf
	
./ttf/ae_Arab.ttf : ./sfd/ae_Arab.sfd ./ttf
	$(CC) ./sfd/ae_Arab.sfd  ./ttf/ae_Arab.ttf
	
./ttf/ae_Cortoba.ttf : ./sfd/ae_Cortoba.sfd ./ttf
	$(CC) ./sfd/ae_Cortoba.sfd  ./ttf/ae_Cortoba.ttf
	
./ttf/ae_Dimnah.ttf : ./sfd/ae_Dimnah.sfd ./ttf
	$(CC) ./sfd/ae_Dimnah.sfd  ./ttf/ae_Dimnah.ttf
	
./ttf/ae_Electron.ttf : ./sfd/ae_Electron.sfd ./ttf
	$(CC) ./sfd/ae_Electron.sfd  ./ttf/ae_Electron.ttf
	
./ttf/ae_Furat.ttf : ./sfd/ae_Furat.sfd ./ttf
	$(CC) ./sfd/ae_Furat.sfd  ./ttf/ae_Furat.ttf
	
./ttf/ae_Granada.ttf : ./sfd/ae_Granada.sfd ./ttf
	$(CC) ./sfd/ae_Granada.sfd  ./ttf/ae_Granada.ttf
	
./ttf/ae_Graph.ttf : ./sfd/ae_Graph.sfd ./ttf
	$(CC) ./sfd/ae_Graph.sfd  ./ttf/ae_Graph.ttf
	
./ttf/ae_Hani.ttf : ./sfd/ae_Hani.sfd ./ttf
	$(CC) ./sfd/ae_Hani.sfd  ./ttf/ae_Hani.ttf
	
./ttf/ae_Haramain.ttf : ./sfd/ae_Haramain.sfd ./ttf
	$(CC) ./sfd/ae_Haramain.sfd  ./ttf/ae_Haramain.ttf
	
./ttf/ae_Hor.ttf : ./sfd/ae_Hor.sfd ./ttf
	$(CC) ./sfd/ae_Hor.sfd  ./ttf/ae_Hor.ttf
	
./ttf/ae_Japan.ttf : ./sfd/ae_Japan.sfd ./ttf
	$(CC) ./sfd/ae_Japan.sfd  ./ttf/ae_Japan.ttf
./ttf/ae_Jet.ttf : ./sfd/ae_Jet.sfd ./ttf
	$(CC) ./sfd/ae_Jet.sfd  ./ttf/ae_Jet.ttf
	
./ttf/ae_Kayrawan.ttf : ./sfd/ae_Kayrawan.sfd ./ttf
	$(CC) ./sfd/ae_Kayrawan.sfd  ./ttf/ae_Kayrawan.ttf
	
./ttf/ae_Khalid.ttf : ./sfd/ae_Khalid.sfd ./ttf
	$(CC) ./sfd/ae_Khalid.sfd  ./ttf/ae_Khalid.ttf
	
./ttf/ae_Mashq-Bold.ttf : ./sfd/ae_Mashq-Bold.sfd ./ttf
	$(CC) ./sfd/ae_Mashq-Bold.sfd  ./ttf/ae_Mashq-Bold.ttf
	
./ttf/ae_Mashq.ttf : ./sfd/ae_Mashq.sfd ./ttf
	$(CC) ./sfd/ae_Mashq.sfd  ./ttf/ae_Mashq.ttf
	
./ttf/ae_Metal.ttf : ./sfd/ae_Metal.sfd ./ttf
	$(CC) ./sfd/ae_Metal.sfd  ./ttf/ae_Metal.ttf
	
./ttf/ae_Nada.ttf : ./sfd/ae_Nada.sfd ./ttf
	$(CC) ./sfd/ae_Nada.sfd  ./ttf/ae_Nada.ttf
	
./ttf/ae_Nagham.ttf : ./sfd/ae_Nagham.sfd ./ttf
	$(CC) ./sfd/ae_Nagham.sfd  ./ttf/ae_Nagham.ttf
	
./ttf/ae_Nice.ttf : ./sfd/ae_Nice.sfd ./ttf
	$(CC) ./sfd/ae_Nice.sfd  ./ttf/ae_Nice.ttf
	
./ttf/ae_Ostorah.ttf : ./sfd/ae_Ostorah.sfd ./ttf
	$(CC) ./sfd/ae_Ostorah.sfd  ./ttf/ae_Ostorah.ttf
	
./ttf/ae_Ouhod-Bold.ttf : ./sfd/ae_Ouhod-Bold.sfd ./ttf
	$(CC) ./sfd/ae_Ouhod-Bold.sfd  ./ttf/ae_Ouhod-Bold.ttf
	
./ttf/ae_Petra.ttf : ./sfd/ae_Petra.sfd ./ttf
	$(CC) ./sfd/ae_Petra.sfd  ./ttf/ae_Petra.ttf
	
./ttf/ae_Rasheeq-Bold.ttf : ./sfd/ae_Rasheeq-Bold.sfd ./ttf
	$(CC) ./sfd/ae_Rasheeq-Bold.sfd  ./ttf/ae_Rasheeq-Bold.ttf
	
./ttf/ae_Rehan.ttf : ./sfd/ae_Rehan.sfd ./ttf
	$(CC) ./sfd/ae_Rehan.sfd  ./ttf/ae_Rehan.ttf
	
./ttf/ae_Salem.ttf : ./sfd/ae_Salem.sfd ./ttf
	$(CC) ./sfd/ae_Salem.sfd  ./ttf/ae_Salem.ttf
	
./ttf/ae_Shado.ttf : ./sfd/ae_Shado.sfd ./ttf
	$(CC) ./sfd/ae_Shado.sfd  ./ttf/ae_Shado.ttf
	
./ttf/ae_Sharjah.ttf : ./sfd/ae_Sharjah.sfd ./ttf
	$(CC) ./sfd/ae_Sharjah.sfd  ./ttf/ae_Sharjah.ttf
	
./ttf/ae_Sindbad.ttf : ./sfd/ae_Sindbad.sfd ./ttf
	$(CC) ./sfd/ae_Sindbad.sfd  ./ttf/ae_Sindbad.ttf
	
./ttf/ae_Tarablus.ttf : ./sfd/ae_Tarablus.sfd ./ttf
	$(CC) ./sfd/ae_Tarablus.sfd  ./ttf/ae_Tarablus.ttf
	
./ttf/ae_Tholoth.ttf : ./sfd/ae_Tholoth.sfd ./ttf
	$(CC) ./sfd/ae_Tholoth.sfd  ./ttf/ae_Tholoth.ttf


# Creating our dist package
dist: $(FONTS) $(ESSENTIALS)
	tar cvfj ae_fonts.tar.bz2 ae_fonts
  
# Creating object files

ae_fonts/AAHS: $(ESSENTIALS) ae_fonts
	mkdir -p ae_fonts/AAHS
ae_fonts/AGA: $(ESSENTIALS) ae_fonts
	mkdir -p ae_fonts/AGA
ae_fonts/FS: $(ESSENTIALS) ae_fonts
	mkdir -p ae_fonts/FS	
ae_fonts/Kasr: $(ESSENTIALS) ae_fonts
	mkdir -p ae_fonts/Kasr
ae_fonts/MCS: $(ESSENTIALS) ae_fonts
	mkdir -p ae_fonts/MCS
ae_fonts/Shmookh: $(ESSENTIALS) ae_fonts
	mkdir -p ae_fonts/Shmookh

ae_fonts/COPYING: COPYING ae_fonts
	cp COPYING ae_fonts
ae_fonts/README: README ae_fonts
	cp README ae_fonts

ae_fonts:
	mkdir -p ae_fonts

ae_fonts/Kasr/ae_AlArabiya.ttf : ./sfd/ae_AlArabiya.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlArabiya.sfd  ./ae_fonts/Kasr/ae_AlArabiya.ttf
	
ae_fonts/Kasr/ae_AlBattar.ttf : ./sfd/ae_AlBattar.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlBattar.sfd  ./ae_fonts/Kasr/ae_AlBattar.ttf
	
ae_fonts/Kasr/ae_AlHor.ttf : ./sfd/ae_AlHor.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlHor.sfd  ./ae_fonts/Kasr/ae_AlHor.ttf
	
ae_fonts/Kasr/ae_AlManzomah.ttf : ./sfd/ae_AlManzomah.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlManzomah.sfd  ./ae_fonts/Kasr/ae_AlManzomah.ttf
	
ae_fonts/Kasr/ae_AlMateen-Bold.ttf : ./sfd/ae_AlMateen-Bold.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlMateen-Bold.sfd  ./ae_fonts/Kasr/ae_AlMateen-Bold.ttf
	
ae_fonts/Kasr/ae_AlMohanad.ttf : ./sfd/ae_AlMohanad.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlMohanad.sfd  ./ae_fonts/Kasr/ae_AlMohanad.ttf
	
ae_fonts/Kasr/ae_AlMothnna-Bold.ttf : ./sfd/ae_AlMothnna-Bold.sfd ae_fonts/Kasr
	$(CC) ./sfd/ae_AlMothnna-Bold.sfd  ./ae_fonts/Kasr/ae_AlMothnna-Bold.ttf
	
ae_fonts/Shmookh/ae_AlYarmook.ttf : ./sfd/ae_AlYarmook.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_AlYarmook.sfd  ./ae_fonts/Shmookh/ae_AlYarmook.ttf
	
ae_fonts/FS/ae_Arab.ttf : ./sfd/ae_Arab.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Arab.sfd  ./ae_fonts/FS/ae_Arab.ttf
	
ae_fonts/AAHS/ae_Cortoba.ttf : ./sfd/ae_Cortoba.sfd ae_fonts/AAHS
	$(CC) ./sfd/ae_Cortoba.sfd  ./ae_fonts/AAHS/ae_Cortoba.ttf
	
ae_fonts/AGA/ae_Dimnah.ttf : ./sfd/ae_Dimnah.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Dimnah.sfd  ./ae_fonts/AGA/ae_Dimnah.ttf
	
ae_fonts/MCS/ae_Electron.ttf : ./sfd/ae_Electron.sfd ae_fonts/MCS
	$(CC) ./sfd/ae_Electron.sfd  ./ae_fonts/MCS/ae_Electron.ttf
	
ae_fonts/AGA/ae_Furat.ttf : ./sfd/ae_Furat.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Furat.sfd  ./ae_fonts/AGA/ae_Furat.ttf
	
ae_fonts/AGA/ae_Granada.ttf : ./sfd/ae_Granada.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Granada.sfd  ./ae_fonts/AGA/ae_Granada.ttf
	
ae_fonts/FS/ae_Graph.ttf : ./sfd/ae_Graph.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Graph.sfd  ./ae_fonts/FS/ae_Graph.ttf
	
ae_fonts/Shmookh/ae_Hani.ttf : ./sfd/ae_Hani.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_Hani.sfd  ./ae_fonts/Shmookh/ae_Hani.ttf
	
ae_fonts/MCS/ae_Haramain.ttf : ./sfd/ae_Haramain.sfd ae_fonts/MCS
	$(CC) ./sfd/ae_Haramain.sfd  ./ae_fonts/MCS/ae_Haramain.ttf
	
ae_fonts/MCS/ae_Hor.ttf : ./sfd/ae_Hor.sfd ae_fonts/MCS
	$(CC) ./sfd/ae_Hor.sfd  ./ae_fonts/MCS/ae_Hor.ttf
	
ae_fonts/FS/ae_Japan.ttf : ./sfd/ae_Japan.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Japan.sfd  ./ae_fonts/FS/ae_Japan.ttf
	
ae_fonts/FS/ae_Jet.ttf : ./sfd/ae_Jet.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Jet.sfd  ./ae_fonts/FS/ae_Jet.ttf
	
ae_fonts/AGA/ae_Kayrawan.ttf : ./sfd/ae_Kayrawan.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Kayrawan.sfd  ./ae_fonts/AGA/ae_Kayrawan.ttf
	
ae_fonts/Shmookh/ae_Khalid.ttf : ./sfd/ae_Khalid.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_Khalid.sfd  ./ae_fonts/Shmookh/ae_Khalid.ttf
	
ae_fonts/AGA/ae_Mashq-Bold.ttf : ./sfd/ae_Mashq-Bold.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Mashq-Bold.sfd  ./ae_fonts/AGA/ae_Mashq-Bold.ttf
	
ae_fonts/AGA/ae_Mashq.ttf : ./sfd/ae_Mashq.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Mashq.sfd  ./ae_fonts/AGA/ae_Mashq.ttf
	
ae_fonts/FS/ae_Metal.ttf : ./sfd/ae_Metal.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Metal.sfd  ./ae_fonts/FS/ae_Metal.ttf
	
ae_fonts/AGA/ae_Nada.ttf : ./sfd/ae_Nada.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Nada.sfd  ./ae_fonts/AGA/ae_Nada.ttf
	
ae_fonts/AAHS/ae_Nagham.ttf : ./sfd/ae_Nagham.sfd ae_fonts/AAHS
	$(CC) ./sfd/ae_Nagham.sfd  ./ae_fonts/AAHS/ae_Nagham.ttf
	
ae_fonts/FS/ae_Nice.ttf : ./sfd/ae_Nice.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Nice.sfd  ./ae_fonts/FS/ae_Nice.ttf
	
ae_fonts/AAHS/ae_Ostorah.ttf : ./sfd/ae_Ostorah.sfd ae_fonts/AAHS
	$(CC) ./sfd/ae_Ostorah.sfd  ./ae_fonts/AAHS/ae_Ostorah.ttf
	
ae_fonts/Shmookh/ae_Ouhod-Bold.ttf : ./sfd/ae_Ouhod-Bold.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_Ouhod-Bold.sfd  ./ae_fonts/Shmookh/ae_Ouhod-Bold.ttf
	
ae_fonts/AGA/ae_Petra.ttf : ./sfd/ae_Petra.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Petra.sfd  ./ae_fonts/AGA/ae_Petra.ttf
	
ae_fonts/AGA/ae_Rasheeq-Bold.ttf : ./sfd/ae_Rasheeq-Bold.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Rasheeq-Bold.sfd  ./ae_fonts/AGA/ae_Rasheeq-Bold.ttf
	
ae_fonts/Shmookh/ae_Rehan.ttf : ./sfd/ae_Rehan.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_Rehan.sfd  ./ae_fonts/Shmookh/ae_Rehan.ttf
	
ae_fonts/FS/ae_Salem.ttf : ./sfd/ae_Salem.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Salem.sfd  ./ae_fonts/FS/ae_Salem.ttf
	
ae_fonts/FS/ae_Shado.ttf : ./sfd/ae_Shado.sfd ae_fonts/FS
	$(CC) ./sfd/ae_Shado.sfd  ./ae_fonts/FS/ae_Shado.ttf
	
ae_fonts/Shmookh/ae_Sharjah.ttf : ./sfd/ae_Sharjah.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_Sharjah.sfd  ./ae_fonts/Shmookh/ae_Sharjah.ttf
	
ae_fonts/AGA/ae_Sindbad.ttf : ./sfd/ae_Sindbad.sfd ae_fonts/AGA
	$(CC) ./sfd/ae_Sindbad.sfd  ./ae_fonts/AGA/ae_Sindbad.ttf
	
ae_fonts/Shmookh/ae_Tarablus.ttf : ./sfd/ae_Tarablus.sfd ae_fonts/Shmookh
	$(CC) ./sfd/ae_Tarablus.sfd  ./ae_fonts/Shmookh/ae_Tarablus.ttf
	
ae_fonts/MCS/ae_Tholoth.ttf : ./sfd/ae_Tholoth.sfd ae_fonts/MCS
	$(CC) ./sfd/ae_Tholoth.sfd  ./ae_fonts/MCS/ae_Tholoth.ttf

all: fonts dist

# Cleaning old files before new make
clean:
	rm -f ae_fonts.tar.bz2 core
	rm -rf ae_fonts ttf
