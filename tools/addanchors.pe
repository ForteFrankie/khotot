#!/usr/bin/env fontforge
# This script adds anchor points needed for Arabic marks 
# positioning, those anchor points MUST be edited by hand 
# after that to give them the proper position.
# You will need to process the font with addlookups.pe 
# script prior to using this one.

if ($version < "20070501")
  Error("Your version of FontForge is too old - \
        20070501 or newer is required");
endif

i=1
while ( i < $argc )
  Open($argv[i])
  Reencode("unicode")
  # Deal with base glyphs
  Select("u+0621", "u+063a", "u+0641", "u+064a", "u+066e", "u+066f")
  SelectMore("u+0670", "u+06d5", "u+0750", "u+076d", "u+fb50", "u+fbb1")
  SelectMore("u+fbd3", "u+fbe9", "u+fe80", "u+fef4", "u+fea0", "u+fef4")
  foreach
    if ( WorthOutputting() == 1) 
      #the X and Y values are choosed roughly
      ya  = Int(GlyphInfo("BBox")[3] + 250)
      yb  = Int(GlyphInfo("BBox")[1] - 250)
      x = Int(GlyphInfo("Width") / 2)
      AddAnchorPoint("ArabicAbove", "basechar", x, ya)
      AddAnchorPoint("ArabicBelow", "basechar", x, yb)
    endif
  endloop
  # Deal with ligatures composed of 2 letters
  Select("u+fbea", "u+fc5d", "u+fc64", "u+fcf1", "u+fcf5", "u+fd3b")
  Select("u+fef5", "ufefc")
  foreach
    if ( WorthOutputting() == 1)
      ya  = Int(GlyphInfo("BBox")[3] + 100)
      yb  = Int(GlyphInfo("BBox")[1] - 100)
      AddAnchorPoint("ArabicAboveLigature", "baselig", 700, ya, 0)
      AddAnchorPoint("ArabicAboveLigature", "baselig", 300, ya, 1)
      AddAnchorPoint("ArabicBelowLigature", "baselig", 700, yb, 0)
      AddAnchorPoint("ArabicBelowLigature", "baselig", 300, yb, 1)
    endif
  endloop
  # Deal with ligatures composed of 3 letters 
  # (We don't have any of it in our fonts, until now)
  Select("u+fd50", "u+fd8f", "u+fd92", "u+fdc7")
  foreach
    if ( WorthOutputting() == 1)
      ya  = Int(GlyphInfo("BBox")[3] + 100)
      yb  = Int(GlyphInfo("BBox")[1] - 100)
      AddAnchorPoint("ArabicAboveLigature", "baselig", 940, ya, 0)
      AddAnchorPoint("ArabicAboveLigature", "baselig", 640, ya, 1)
      AddAnchorPoint("ArabicAboveLigature", "baselig", 340, ya, 2)
      AddAnchorPoint("ArabicBelowLigature", "baselig", 940, yb, 0)
      AddAnchorPoint("ArabicBelowLigature", "baselig", 640, yb, 1)
      AddAnchorPoint("ArabicBelowLigature", "baselig", 340, yb, 2)
    endif
  endloop
  # Deal with marks that lay above the base glyph
  # 
  # special case for shadda, hamza above and madda above
  SelectSingletons("u+0651", "u+0653", "u+0654")
  foreach
    if ( WorthOutputting() == 1)
      AddAnchorPoint("ArabicAbove", "mark", 300, 1650)
      AddAnchorPoint("ArabicAboveLigature", "mark", 300, 1650)
      #AddAnchorPoint("ArabicMark2MarkAbove", "basemark", 340, 1650)
      #AddAnchorPoint("ArabicMark2MarkBelow", "basemark", 340, 1550)
    endif
  endloop
  SelectSingletons("u+064b", "u+064c", "u+064e", "u+064f", "u+0652")
  SelectMoreSingletons("u+0657", "u+0658", "u+0659", "u+065a", "u+065b")
  SelectMoreSingletons("u+065d", "u+065e", "u+0670", "u+06e0", "u+06e1")
  SelectMoreSingletons("u+06e2", "u+06e4", "u+06e7", "u+06e8", "u+06eb", "u+06ec")
  foreach
    if ( WorthOutputting() == 1)
      AddAnchorPoint("ArabicAbove", "mark", 230, 1275)
      AddAnchorPoint("ArabicAboveLigature", "mark", 230, 1275)
      #AddAnchorPoint("ArabicMark2MarkAbove", "mark", 340, 1550)
    endif
  endloop
  # Deal with marks that lay below the base glyph.
  # 
  # special case for hamza below
  SelectSingletons("u+0655")
    if ( WorthOutputting() == 1)
      AddAnchorPoint("ArabicBelow", "mark", 165, -500)
      AddAnchorPoint("ArabicBelowLigature", "mark", 165, -500)
      #AddAnchorPoint("ArabicMark2MarkBelow", "basemark", 190, -400)
    endif
  SelectSingletons("u+064d", "u+0650", "u+0656", "u+065c")
  SelectMoreSingletons("u+06e3", "u+06ea", "u+06ed")
  foreach
    if ( WorthOutputting() == 1)
      AddAnchorPoint("ArabicBelow", "mark", 165, -500)
      AddAnchorPoint("ArabicBelowLigature", "mark", 165, -500)
      #AddAnchorPoint("ArabicMark2MarkBelow", "mark", 190, -400)
    endif
  endloop
  
  Save()
  i++
endloop
